package javaSrv;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JavaServlet extends HttpServlet
{

	private static final long serialVersionUID = 1L;
    public JavaServlet(){}
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {	
    	//in
    	try{
    	String data = request.getParameter("data");
    	//app
    	StringBuffer rslt = new StringBuffer();
    	
    	if(data != null){
    		if(!data.equals("")){
    			//app 
        		//rslt.append(App.run(data));
    			rslt.append("Hello from Java<BR>");
    			rslt.append(" Get request with data: "+data);
        		JavaServlet.reply(response, rslt);
    		} else {
    			//400
    			JavaServlet.reply(response,"We got bad request(aka error 400)",HttpServletResponse.SC_BAD_REQUEST);
    		}
    		
    	} else {
    		//Nothing above received
    		if(ServerConf.getDebug() == 1){
    			JavaServlet.debug(request);
    		}
    		JavaServlet.reply(response,"We got bad request(aka error 400)",HttpServletResponse.SC_BAD_REQUEST);
    	}

    	}catch(NullPointerException e){
    		JavaServlet.reply(response,"We got bad request(aka error 400)",HttpServletResponse.SC_BAD_REQUEST);
    	}catch (Exception e) {
    		 e.printStackTrace();
    		 JavaServlet.reply(response,"Server is temporary down, sorry",HttpServletResponse.SC_SERVICE_UNAVAILABLE);
		}
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {	
    	//in
    	try{
    	String data = request.getParameter("data");
    	//app
    	StringBuffer rslt = new StringBuffer();
    	
    	if(data != null){
    		if(!data.equals("")){
    			//app 
        		//rslt.append(App.run(data));
    			rslt.append("Hello from Java<BR>");
    			rslt.append(" Post request wit data: "+data);
        		JavaServlet.reply(response, rslt);
    		} else {
    			//400
    			JavaServlet.reply(response,"We got bad request(aka error 400)",HttpServletResponse.SC_BAD_REQUEST);
    		}
    		
    	} else {
    		//Nothing above received
    		if(ServerConf.getDebug() == 1){
    			JavaServlet.debug(request);
    		}
    		JavaServlet.reply(response,"We got bad request(aka error 400)",HttpServletResponse.SC_BAD_REQUEST);
    	}

    	}catch(NullPointerException e){
    		JavaServlet.reply(response,"We got bad request(aka error 400)",HttpServletResponse.SC_BAD_REQUEST);
    	}catch (Exception e) {
    		 e.printStackTrace();
    		 JavaServlet.reply(response,"Server is temporary down, sorry",HttpServletResponse.SC_SERVICE_UNAVAILABLE);
		}
    }
    private static void reply(HttpServletResponse response, StringBuffer rslt) throws IOException{
    	String result = rslt.toString();
    	JavaServlet.reply(response, result,HttpServletResponse.SC_OK);
    }
    private static void reply(HttpServletResponse response, String rslt,int status) throws IOException{
    	response.setContentType("text/html");
        response.setStatus(status);
        response.getWriter().println(rslt);
    }
    private static void debug(HttpServletRequest request){
    	System.err.println("400 - We got: "+request.getQueryString()); //just for debug
    }
}