package javaSrv;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OneServlet extends HttpServlet {
	private String greet="Hello Java World";
	
	public OneServlet(){}
	public OneServlet(String greet){
		this.greet = greet;
	}
	
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		response.setContentType("text/html");
        response.setStatus(200);
        response.getWriter().println("<h1>"+this.greet+"</h1>");
	}
}
